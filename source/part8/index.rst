.. _part8:

*************************************************************************************************
Partie 8 | Traits and monads
*************************************************************************************************

Question proposed by Group 15, Florian Duprez and Yorick Tixhon
=====================================================================

Question 1 What are the differences between Traits and Abstrat class? Show a case where you should favorise Traits.
------------


Question 2 What is a monad? What are the 3 monad laws? Give examples
------------


Question 3 A is an array, and B is a List. If you execute this line, what is the output type?:
------------
.. code-block:: scala

	for( i <- B; <- A ) yield (i,j).

Question 4 Rewrite this for loop using only foreach, map, withFilter and flatMap
------------
.. code-block:: scala

	for (i <- list, j <- array , if (i % 2 == 0)){
		yield i*j
	}

Question 5 Implements l.map(f) with flatMap
------------
