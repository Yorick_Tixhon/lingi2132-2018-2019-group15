

.. highlight:: java
   :linenothreshold: 4


###################################################
LINGI2132: Questions Exam Proposal
###################################################

.. toctree::
   :maxdepth: 2

   part1/index
   part2/index
   part3/index
   part4/index
   part5/index
   part6/index
   part7/index

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

